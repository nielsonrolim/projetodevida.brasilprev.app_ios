//
//  MLUtil.m
//  RetailController
//
//  Created by Delano Oliveira on 14/08/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLUtil.h"

@implementation MLUtil

//+ (void)showAlertMessage:(NSString *)message withTitle:(NSString *)title
//{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"ALERT_BUTTON_OK", @"Button OK") otherButtonTitles:nil];
//    [alertView show];
//}
//
//+ (void)showAlertMessage:(NSString *)message
//{
//    [MLUtil showAlertMessage:message withTitle:NSLocalizedString(@"ALERT_DEFAULT_TITLE", @"Default Alert Title")];
//}

//+ (UIAlertView*) confirmDialog:(NSString*) message withTitle:(NSString*) title cancelText:(NSString*)cancelString andConfirmText:(NSString*)confirmString  {
//    UIAlertView* confirm = [[UIAlertView alloc] init];
//    confirm.title = title;
//    confirm.message = message;
//    [confirm addButtonWithTitle:cancelString];
//    [confirm addButtonWithTitle:confirmString];
//    return confirm;
//}
//
//+ (UIAlertView*) confirmDialog:(NSString*) message {
//    UIAlertView* confirm = [MLUtil confirmDialog:message
//                                       withTitle:NSLocalizedString(@"CONFIRM_DEFAULT_TITLE", @"Default Confirm Title")
//                                      cancelText:NSLocalizedString(@"CONFIRM_DEFAULT_NO", @"Default Cancel Text")
//                                  andConfirmText:NSLocalizedString(@"CONFIRM_DEFAULT_YES", @"Default Confirm Text")];
//    return confirm;
//}
//
//+ (UIAlertView*) confirmDialog {
//    UIAlertView* confirm = [MLUtil confirmDialog:NSLocalizedString(@"CONFIRM_DEFAULT_MESSAGE", @"Default Confirm Message")
//                                       withTitle:NSLocalizedString(@"CONFIRM_DEFAULT_TITLE", @"Default Confirm Title")
//                                      cancelText:NSLocalizedString(@"CONFIRM_DEFAULT_NO", @"Default Cancel Text")
//                                  andConfirmText:NSLocalizedString(@"CONFIRM_DEFAULT_YES", @"Default Confirm Text")];
//    return confirm;
//}

+ (void) reposition:(UIView*)element increasingXBy:(int)xPts andYBy:(int)yPts {
    int x = element.frame.origin.x;
    int y = element.frame.origin.y;
    int width = element.frame.size.width;
    int height = element.frame.size.height;
    
    int new_x = x + xPts;
    int new_y = y + yPts;
    
    element.frame = CGRectMake(new_x, new_y, width, height);
}

+ (void) move:(UIView*)view ToX:(int)newX {
    [self move:view ToX:newX ToY:view.frame.origin.y];
}

+ (void) move:(UIView*)view ToY:(int)newY {
    [self move:view ToX:view.frame.origin.x ToY:newY];
}

+ (void) move:(UIView*)view ToX:(int)newX ToY:(int)newY {
    view.frame = CGRectMake(newX, newY, view.frame.size.width, view.frame.size.height);
}

+ (void) resize:(UIView*)element incresingWidthBy:(int)widthPts andHeightBy:(int)heightPts {
    int x = element.frame.origin.x;
    int y = element.frame.origin.y;
    int width = element.frame.size.width;
    int height = element.frame.size.height;
    
    int new_width = width + widthPts;
    int new_height = height + heightPts;
    
    element.frame = CGRectMake(x, y, new_width, new_height);
}

+ (NSMutableString*) formatString:(NSString*)string WithMask:(NSString*)filter {
    NSUInteger onOriginal = 0, onFilter = 0, onOutput = 0;
    char outputString[([filter length])];
    BOOL done = NO;
    
    while(onFilter < [filter length] && !done)
    {
        char filterChar = [filter characterAtIndex:onFilter];
        char originalChar = onOriginal >= string.length ? '\0' : [string characterAtIndex:onOriginal];
        switch (filterChar) {
            case '#':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isdigit(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            default:
                // Any other character will automatically be inserted for the user as they type (spaces, - etc..) or deleted as they delete if there are more numbers to come.
                outputString[onOutput] = filterChar;
                onOutput++;
                onFilter++;
                if(originalChar == filterChar)
                    onOriginal++;
                break;
        }
    }
    outputString[onOutput] = '\0'; // Cap the output string
    return [NSMutableString stringWithUTF8String:outputString];
}

+ (NSString*) cleanCNPJorCPF:(NSString*)maskedNumber {
    NSString *cleaned = maskedNumber;
    cleaned = [cleaned stringByReplacingOccurrencesOfString:@"." withString:@""];
    cleaned = [cleaned stringByReplacingOccurrencesOfString:@"-" withString:@""];
    cleaned = [cleaned stringByReplacingOccurrencesOfString:@"/" withString:@""];
    return cleaned;
}

+ (NSString*) formatCNPJorCPF:(NSString*)number {
    NSString* formattedString = @"";
    if (number.length == 11) {  // username is a CPF
        NSString* filter = @"###.###.###-##";
        formattedString = [self formatString:number WithMask:filter];
    } else if (number.length == 14) { // username is a CNPJ
        NSString* filter = @"##.###.###/####-##";
        formattedString = [self formatString:number WithMask:filter];
    }
    return formattedString;
}

+ (BOOL) validateEmail:(NSString*) emailAddress {
    
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern
                                                                      options:NSRegularExpressionCaseInsensitive
                                                                        error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailAddress
                                                     options:0
                                                       range:NSMakeRange(0, [emailAddress length])];
    
    BOOL valid = (regExMatches == 0) ? NO : YES;
//    NSLog(@"valid email? %d", valid);

    return valid;
}

+ (UIImage*) imageFromURL:(NSString*)url {
    NSURL *imageURL = [NSURL URLWithString:url];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image = [UIImage imageWithData:imageData];
    return image;
}

+ (void) scrollToBottom:(UIScrollView*)scrollView {
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
    [scrollView setContentOffset:bottomOffset animated:NO];
}

+ (NSString*) formatCurrency:(NSNumber*)value {
    return [MLUtil formatCurrency:value hasDecimal:YES];
}

+ (NSString*) formatCurrency:(NSNumber*)value hasDecimal:(BOOL)hasDecimal {
    //Formatter for current currency
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    //    formatter.locale = [NSLocale currentLocale];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"pt_BR"];
    formatter.generatesDecimalNumbers = YES;
    if (hasDecimal) {
        formatter.maximumFractionDigits = 2;
        formatter.minimumFractionDigits = 2;
        formatter.alwaysShowsDecimalSeparator = YES;
    } else {
        formatter.maximumFractionDigits = 0;
        formatter.minimumFractionDigits = 0;
        formatter.alwaysShowsDecimalSeparator = NO;
    }
    formatter.maximumIntegerDigits = 12;
    
    return [formatter stringFromNumber:value];
}

@end
