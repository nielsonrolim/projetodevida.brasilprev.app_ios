//
//  TableViewController+CustomColor.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "UITabBarController+CustomColor.h"

@implementation UITabBarController (CustomColor)

- (void) paintTabBar {
    //Adjusting color of tab bar items
    NSDictionary* inactiveColorAttribute = [NSDictionary dictionaryWithObjectsAndKeys:INACTIVE_COLOR, NSForegroundColorAttributeName,nil];
    NSDictionary* activeColorAttribute = [NSDictionary dictionaryWithObjectsAndKeys:ACTIVE_COLOR, NSForegroundColorAttributeName,nil];
    
    UITabBar *tabBar = self.tabBar;
    for (UITabBarItem* tabBarItem in [tabBar items]) {
        tabBarItem.image = [tabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabBarItem.selectedImage = [tabBarItem.selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [tabBarItem setTitleTextAttributes:inactiveColorAttribute forState:UIControlStateNormal];
        [tabBarItem setTitleTextAttributes:activeColorAttribute forState:UIControlStateSelected];
    }
    
}

@end
