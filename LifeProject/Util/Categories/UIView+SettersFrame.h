//
//  UIView+SettersFrame.h
//  TouchCare
//
//  Created by Delano Oliveira on 30/09/14.
//  Copyright (c) 2014 I/O Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SettersFrame)

@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;


@end
