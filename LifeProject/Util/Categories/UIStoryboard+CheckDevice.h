//
//  UIStoryboard+CheckDevice.h
//  TouchCare
//
//  Created by Delano Oliveira on 30/09/14.
//  Copyright (c) 2014 I/O Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (CheckDevice)

+ (UIStoryboard *)storyboardWithNameAutoSelectDevice:(NSString *)name bundle:(NSBundle *)storyboardBundleOrNil;

@end
