//
//  UIStoryboard+CheckDevice.m
//  TouchCare
//
//  Created by Delano Oliveira on 30/09/14.
//  Copyright (c) 2014 I/O Labs. All rights reserved.
//

#import "UIStoryboard+CheckDevice.h"

@implementation UIStoryboard (CheckDevice)

+ (UIStoryboard *)storyboardWithNameAutoSelectDevice:(NSString *)name bundle:(NSBundle *)storyboardBundleOrNil
{
    if(IS_IPAD){
        name = [name stringByAppendingString:@"~iPad"];
    }else{
        name = [name stringByAppendingString:@"~iPhone"];
    }
    return [UIStoryboard storyboardWithName:name bundle:storyboardBundleOrNil];
}

@end
