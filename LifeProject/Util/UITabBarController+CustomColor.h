//
//  TableViewController+CustomColor.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+Helper.h"

//Inactive and active color attributes
#define INACTIVE_COLOR  [UIColor colorRgbWithRed:153  green:204 blue:255 alpha:1]
#define ACTIVE_COLOR    [UIColor whiteColor]

@interface UITabBarController (CustomColor)

- (void) paintTabBar;

@end
