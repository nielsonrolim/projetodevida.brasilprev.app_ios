//
//  UIColor+Helper.h
//  LifeProject
//
//  Created by Nielson Rolim on 10/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface UIColor (Helper)

+ (UIColor *)colorWithRGBA:(NSUInteger)color;
+ (UIColor *) colorRgbWithRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(NSInteger)alpha;

@end
