//
//  MLEmailAddressTextField.h
//  RetailController
//
//  Created by Nielson Rolim on 8/15/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLTextField.h"

/*
 A custom TextField to input done button in NumberPad
 */

@interface MLNumberPadTextField : MLTextField

@end
