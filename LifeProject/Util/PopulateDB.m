//
//  PopulateDB.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/5/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "PopulateDB.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>


@interface PopulateDB ()

@property (nonatomic, weak) AppDelegate* appDelegate;

@property (nonatomic, strong) NSEntityDescription *categoryDescription;
@property (nonatomic, strong) NSEntityDescription *projectDescription;

@end

@implementation PopulateDB

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (NSEntityDescription*) categoryDescription {
    if (!_categoryDescription) {
        _categoryDescription = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:self.appDelegate.managedObjectContext];
    }
    return _categoryDescription;
}

- (NSEntityDescription*) projectDescription {
    if (!_projectDescription) {
        _projectDescription = [NSEntityDescription entityForName:@"Project" inManagedObjectContext:self.appDelegate.managedObjectContext];
    }
    return _projectDescription;
}

- (void) populate {
    
    [self deletePreviousData];
    
    //Adding Projects
    NSArray* projects = nil;
    NSManagedObject *category = nil;
    
    //Projetos Para Você
    //Viagem
    projects = @[
                 @[@"Cruzeiro volta ao mundo", @"iphone_voce_viagem_cruzeiro", @50000],
                 @[@"Excursão à lua", @"iphone_voce_viagem_lua", @600000],
                 @[@"Assistir a uma final de Roland Garros", @"iphone_voce_viagem_grandslam", @17000],
                 @[@"Assistir a corridas de F-1 em Mônaco", @"iphone_voce_viagem_f1", @35000],
                 @[@"2ª Lua de Mel Resort de Luxo Ilhas Maldivas", @"iphone_voce_viagem_2luademel", @40000],
                 @[@"Conhecer todas as praias caribenhas", @"iphone_voce_viagem_ilhas", @54000],
                 @[@"Nepal, Butão e Tibete", @"iphone_voce_nepal2", @16000, @"white"]
                ];
    category = [self addCategory:@"Viagem" title:@"Para onde você gostaria de viajar?" titleColor:@"blue" buttonImage:@"button_background_travel" backgroundImage:@"background_travel" type:@"forYou"];
    [self addProjetcs:projects inCategory:category];
    
    //Intercâmbio
    projects = @[
                 @[@"Canadá - Vancouver", @"iphone_voce_intercambio_canada", @32000, @"blue"],
                 @[@"Austrália - Sidney", @"iphone_voce_intercambio_australia", @35000],
                 @[@"Irlanda - Dublin", @"iphone_voce_intercambio_irlanda", @33000],
                 @[@"Nova Zelândia - Auckland", @"iphone_voce_intercambio_zelandia", @26000],
                 @[@"EUA - Nova York", @"iphone_voce_intercambio_eua", @46000],
                 @[@"Inglaterra – Londres", @"iphone_voce_intercambio_inglaterra", @47000],
                 @[@"França - Paris", @"iphone_voce_intercambio_franca", @40000],
                 @[@"Alemanha – Munique", @"iphone_voce_intercambio_Alemanha", @34000],
                 @[@"Itália – Milão", @"iphone_voce_intercambio_Italia", @43000],
                 @[@"Espanha – Barcelona", @"iphone_voce_intercambio_Espanha", @31000],
                 @[@"China - Pequim", @"iphone_voce_intercambio_China", @34000]
                ];
    category = [self addCategory:@"Intercâmbio" title:@"Para qual país você gostaria de ir?" titleColor:@"white" buttonImage:@"button_background_study_abroad" backgroundImage:@"background_study_abroad" type:@"forYou"];
    [self addProjetcs:projects inCategory:category];
    
    //MBA Exterior
    projects = @[
                 @[@"Harvard", @"iphone_voce_mba_Harvard", @229000],
                 @[@"Universidade de Stanford", @"iphone_voce_mba_Stanford", @239000],
                 @[@"London Business School", @"iphone_voce_mba_LongoColumbiaMIT", @195000, @"blue"],
                 @[@"Columbia Business School", @"iphone_voce_mba_LongoColumbiaMIT", @232000],
                 @[@"MIT", @"iphone_voce_mba_LongoColumbiaMIT", @230000]
                ];
    category = [self addCategory:@"MBA Exterior" title:@"Em qual destas universidades você gostaria de estudar?" titleColor:@"blue" buttonImage:@"button_background_mba" backgroundImage:@"background_mba" type:@"forYou"];
    [self addProjetcs:projects inCategory:category];

    //Independência Financeira
    projects = @[
                 @[@"Ter primeiro milhão", @"iphone_voce_filho_milhao", @1000000],
                 @[@"Aposentadoria", @"iphone_voce_indfinanceira_Aposentadoria", @1000000, @"blue"]
                 ];
    category = [self addCategory:@"Independência Financeira" title:@"Qual destes projetos você gostaria de realizar?" titleColor:@"blue" buttonImage:@"button_background_financial_independence" backgroundImage:@"background_financial_independence" type:@"forYou"];
    [self addProjetcs:projects inCategory:category];

    //Negócio Próprio
    projects = @[
                 @[@"Franquia de R$ 300 mil", @"iphone_voce_negocio_Franquia300mil", @300000],
                 @[@"Cafeteria", @"iphone_voce_negocio_cafeteria", @200000],
                 @[@"Salão de Beleza", @"iphone_voce_negocio_salaodebeleza", @150000, @"blue"],
                 @[@"Restaurante", @"iphone_voce_negocio_restaurante", @500000, @"blue"],
                 @[@"Montar uma pousada na praia", @"iphone_voce_negocio_pousadapraia", @1500000]
                ];
    category = [self addCategory:@"Negócio Próprio" title:@"Que tipo de negócio você gostaria de ter?" titleColor:@"blue" buttonImage:@"button_background_own_business" backgroundImage:@"background_own_business" type:@"forYou"];
    [self addProjetcs:projects inCategory:category];

    //Imóvel
    projects = @[
                 @[@"Casa em Miami Beach", @"iphone_voce_imovel_miami", @1000000],
                 @[@"Casa de praia em Florianópolis beira mar", @"iphone_voce_imovel_casadepraia", @1800000],
                 @[@"Casa de praia em Trancoso beira mar", @"iphone_voce_imovel_casadepraia", @1300000],
                 @[@"Apto em Ipanema de frente para o mar", @"iphone_voce_imovel_Ipanema", @2500000]
                ];
    category = [self addCategory:@"Imóvel" title:@"Qual destes imóveis você gostaria de passar férias ou morar?" titleColor:@"blue" buttonImage:@"button_background_property" backgroundImage:@"background_property" type:@"forYou"];
    [self addProjetcs:projects inCategory:category];
    
    
    //Projetos Para Seu Filho
    //Intercâmbio
    projects = @[
                 @[@"Canadá - Vancouver", @"iphone_voce_intercambio_canada", @113000, @"blue"],
                 @[@"Austrália - Sidney", @"iphone_voce_intercambio_australia", @70000],
                 @[@"Irlanda - Dublin", @"iphone_voce_intercambio_irlanda", @73000],
                 @[@"Nova Zelândia - Auckland", @"iphone_voce_intercambio_zelandia", @78000],
                 @[@"EUA - Nova York", @"iphone_voce_intercambio_eua", @100000],
                 @[@"Inglaterra – Londres", @"iphone_voce_intercambio_inglaterra", @113000],
                 @[@"França - Paris", @"iphone_voce_intercambio_franca", @105000],
                 @[@"Alemanha – Munique", @"iphone_voce_intercambio_Alemanha", @113000],
                 @[@"Itália – Milão", @"iphone_voce_intercambio_Italia", @70000],
                 @[@"Espanha – Barcelona", @"iphone_voce_intercambio_Espanha", @80000],
                 @[@"China – Pequim", @"iphone_voce_intercambio_China", @105000]
                 ];
    category = [self addCategory:@"Intercâmbio" title:@"Em qual destes países você gostaria que seu filho fosse estudar?" titleColor:nil buttonImage:@"button_background_study_abroad" backgroundImage:@"background_study_abroad" type:@"forYourChild"];
    [self addProjetcs:projects inCategory:category];

    //Faculdade
    projects = @[
                 @[@"Engenharia", @"iphone_filho_faculdade_Engenharia", @120000, @"blue"],
                 @[@"Medicina", @"iphone_filho_faculdade_Medicina", @360000],
                 @[@"Publicidade", @"iphone_filho_faculdade_Publicidade", @140000, @"blue"],
                 @[@"Odontologia", @"iphone_filho_faculdade_Odontologia", @65000, @"blue"],
                 @[@"Direito", @"iphone_filho_faculdade_direito", @90000, @"blue"]
                 ];
    category = [self addCategory:@"Faculdade" title:@"Qual destes cursos o seu filho vai cursar? " titleColor:@"blue" buttonImage:@"button_background_university" backgroundImage:@"background_university" type:@"forYourChild"];
    [self addProjetcs:projects inCategory:category];

    //1o negócio
    projects = @[
                 @[@"Consultório médico", @"iphone_filho_negocio_medico", @30000, @"blue"],
                 @[@"Consultório odontológico", @"iphone_filho_faculdade_Odontologia", @50000, @"blue"],
                 @[@"Franquia R$ 100 mil", @"iphone_filho_negocio_franquia", @100000]
                 ];
    category = [self addCategory:@"1° negócio" title:@"Qual destes projetos você gostaria de proporcionar para o seu filho?" titleColor:@"blue" buttonImage:@"button_background_first_business" backgroundImage:@"background_first_business" type:@"forYourChild"];
    [self addProjetcs:projects inCategory:category];

    //Outros
    projects = @[
                 @[@"Ter primeiro milhão", @"iphone_voce_filho_milhao", @1000000],
                 @[@"1° carro", @"iphone_filho_primeirocarro", @35000, @"blue"],
                 @[@"Festa de debutante", @"iphone_filho_debutante", @50000, @"blue"],
                 @[@"Mochilão pela Europa", @"iphone_filho_mochilao", @12000]
                 ];
    category = [self addCategory:@"Outros" title:@"Qual destes projetos você gostaria de proporcionar para o seu filho?" titleColor:@"blue" buttonImage:@"button_background_others" backgroundImage:@"background_others" type:@"forYourChild"];
    [self addProjetcs:projects inCategory:category];
}

- (NSManagedObject*) addCategory:(NSString*)categoryName title:(NSString*)title titleColor:(NSString*)titleColor buttonImage:(NSString*)buttonImageName backgroundImage:(NSString*)backgroundImageName type:(NSString*)type {
    //Adding Category
    NSManagedObject *category = [[NSManagedObject alloc] initWithEntity:self.categoryDescription insertIntoManagedObjectContext:self.appDelegate.managedObjectContext];
    [category setValue:categoryName forKey:@"name"];
    [category setValue:title forKey:@"title"];
    if (titleColor != nil) [category setValue:titleColor forKey:@"titleColor"];
    [category setValue:buttonImageName forKey:@"button_image"];
    [category setValue:backgroundImageName forKey:@"background_image"];
    [category setValue:type forKey:@"type"];

    //Saving
    NSError *error = nil;
    if (![self.appDelegate.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    return category;
}

- (void) addProjetcs:(NSArray*)projects inCategory:(NSManagedObject*)category {
    for (NSArray* p in projects) {
        NSManagedObject *project = [[NSManagedObject alloc] initWithEntity:self.projectDescription insertIntoManagedObjectContext:self.appDelegate.managedObjectContext];
        [project setValue:p[0] forKey:@"title"];
        [project setValue:p[1] forKey:@"background_image"];
        [project setValue:p[2] forKey:@"value"];
        if (p.count > 3) {
            [project setValue:p[3] forKey:@"titleColor"];
        }
        [project setValue:category forKey:@"category"];
    }
    //Saving
    NSError *error = nil;
    if (![self.appDelegate.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

- (void) deletePreviousData {
    NSError *error = nil;
    
    //Deleting Current Categories and Projects (Using Cascating)
    NSFetchRequest * allCategories = [[NSFetchRequest alloc] init];
    [allCategories setEntity:self.categoryDescription];
    [allCategories setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSArray * currentCategories = [self.appDelegate.managedObjectContext executeFetchRequest:allCategories error:&error];
    for (NSManagedObject * category in currentCategories) {
        [self.appDelegate.managedObjectContext deleteObject:category];
    }
    //Saving
    if (![self.appDelegate.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

@end
