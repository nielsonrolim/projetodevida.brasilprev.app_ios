//
//  MLCurrencyField.h
//  RetailController
//
//  Created by Nielson Rolim on 8/27/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLNumberPadTextField.h"

/*
A custom TextField to input done button in NumberPad and use mask for Currency acording user currentLocale
*/

@interface MLCurrencyField : MLNumberPadTextField <UITextFieldDelegate>

//Double value of this text field
@property (assign, nonatomic) double doubleValue;

//Maximum value allowed
@property (assign, nonatomic) double maximumValueAllowed;

@end
