//
//  PopulateDB.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/5/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopulateDB : NSObject

- (void) populate;

@end
