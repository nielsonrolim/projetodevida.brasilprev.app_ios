//
//  MLCurrencyField.m
//  RetailController
//
//  Created by Nielson Rolim on 8/27/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MLCurrencyField.h"

@implementation MLCurrencyField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.delegate = self;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (double) maximumValueAllowed {
    if (!_maximumValueAllowed) {
        _maximumValueAllowed = [@999999999 doubleValue];
    }
    return _maximumValueAllowed;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //Formatter for current currency
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
//    formatter.locale = [NSlocale currentLocale];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"pt_BR"];
    formatter.generatesDecimalNumbers = YES;
    formatter.maximumFractionDigits = 2;
    formatter.minimumFractionDigits = 2;
    formatter.maximumIntegerDigits = 12;
    formatter.alwaysShowsDecimalSeparator = YES;
    
    //Appeding the value of what the user typed to the textField.text
    NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    //Cleaning other characters than numbers
    NSString *stringToBeFormatted = changedString;
    stringToBeFormatted = [stringToBeFormatted stringByReplacingOccurrencesOfString:[formatter currencySymbol] withString:@""];
    stringToBeFormatted = [stringToBeFormatted stringByReplacingOccurrencesOfString:@"." withString:@""];
    stringToBeFormatted = [stringToBeFormatted stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    //Double value of string
    self.doubleValue = ([stringToBeFormatted doubleValue]/100);
    
    if (self.doubleValue <= self.maximumValueAllowed) {
        //Formatting the whole thing
        NSString* formattedString = [formatter stringFromNumber:[NSNumber numberWithDouble:self.doubleValue]];

        //Assigning the formatted string to the textField
        textField.text = formattedString;
    }
    
    
    //If backspace pressed, it delete the last character
//    if ([string isEqualToString:@""]) {
//        return YES;
//    }

    //Otherwise, just ignore what was pressed
    return NO;
}

@end
