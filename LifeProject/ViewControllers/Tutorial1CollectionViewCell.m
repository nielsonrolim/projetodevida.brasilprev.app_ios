//
//  Tutorial1CollectionViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "Tutorial1CollectionViewCell.h"
#import "MLUtil.h"

@interface Tutorial1CollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *buttonForYou;

@property (weak, nonatomic) IBOutlet UIImageView *buttonForYourChild;

@end

@implementation Tutorial1CollectionViewCell

- (void) animateCell {

    //Button For You
    [self animateButtonForYou];
    
    //Button For Your Child
    [self animateButtonForYourChild];
}

- (void) resetCellState {
    int originalXPosition;
    
    if (IS_IPAD) {
        originalXPosition = -353;
    } else {
        originalXPosition = -285;
    }
    
    self.buttonForYou.alpha = 0.0f;
    [MLUtil move:self.buttonForYou ToX:originalXPosition];
    
    self.buttonForYourChild.alpha = 0.0f;
    [MLUtil move:self.buttonForYourChild ToX:originalXPosition];
}

- (void) animateButtonForYou {
    int finalXPosition;

    if (IS_IPAD) {
        finalXPosition = 337;
    } else  {
        finalXPosition = 21;
    }

    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.buttonForYou setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    

    [UIView animateWithDuration:1.0f
                     animations:^{
                         [MLUtil move:self.buttonForYou ToX:finalXPosition];
                     }
     ];
}

- (void) animateButtonForYourChild {
    int finalXPosition;
    
    if (IS_IPAD) {
        finalXPosition = 337;
    } else  {
        finalXPosition = 21;
    }

    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.buttonForYourChild setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    
    [UIView animateWithDuration:1.5f
                     animations:^{
                         [MLUtil move:self.buttonForYourChild ToX:finalXPosition];
                     }
     ];
}

@end
