//
//  OurPlansViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/22/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "OurPlansViewController.h"

@interface OurPlansViewController ()

@property (weak, nonatomic) IBOutlet UITextView *ourPlansTextView;

@end

@implementation OurPlansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!IS_IPHONE) {
        UIImage *image = [UIImage imageNamed:@"logo_brasilprev"];
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    }

    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
