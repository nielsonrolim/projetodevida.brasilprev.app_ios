//
//  ResultViewController.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/10/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "SimulationViewController.h"

@interface ResultViewController : SimulationViewController

//Total amount
@property (strong, nonatomic) NSNumber* totalAmount;

//Monthly Interest Rate
@property (assign, nonatomic) double monthlyInterestRate;

//Slider for interest rate
@property (weak, nonatomic) IBOutlet UISlider *anualInterestRateSlider;

//Label for interest rate
@property (weak, nonatomic) IBOutlet UILabel *anualInterestRateLabel;

//Monthly investment
@property (assign, nonatomic) double monthlyInvestment;

//Chosen years
@property (assign, nonatomic) int years;

//Chosen months
@property (assign, nonatomic) int months;


//Method to convert an anual interest rate to a monthly interest rate
- (double) convertAnualToMonthlyInterestRate:(double)anualInterestRate;

//Mothod to calculate the monthly investiment
- (void) calculateMonthlyInvestiment;

//Method to calculate the saving time
- (void) calculateSavingTime;

//Method trigged when anual interest rate slider has changed
- (IBAction)anualInterestRateChanged:(UISlider *)sender;

@end
