//
//  Tutorial5CollectionViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "Tutorial5CollectionViewCell.h"
#import "MLUtil.h"

@interface Tutorial5CollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *figure1ImageView;

@end

@implementation Tutorial5CollectionViewCell

- (void) animateCell {
    int finalYPosition = (IS_IPAD) ? 150 : 0;
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.figure1ImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    
    [UIView animateWithDuration:0.7f
                     animations:^{
                         [MLUtil move:self.figure1ImageView ToY:finalYPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) resetCellState {
    int originalYPosition = (IS_IPAD) ? 500 : 258;
    self.figure1ImageView.alpha = 0.0f;
    [MLUtil move:self.figure1ImageView ToY:originalYPosition];
}

@end
