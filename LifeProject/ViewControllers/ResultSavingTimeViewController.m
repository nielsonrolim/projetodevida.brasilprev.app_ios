//
//  ResultSavingTimeViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/6/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "ResultSavingTimeViewController.h"

@interface ResultSavingTimeViewController ()

@end

@implementation ResultSavingTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self calculateMonthlyInvestiment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)anualInterestRateChanged:(UISlider *)sender {
    [super anualInterestRateChanged:sender];
    [self calculateMonthlyInvestiment];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
