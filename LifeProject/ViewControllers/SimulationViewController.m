//
//  SimulationViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/22/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "SimulationViewController.h"
#import "CategoriesViewController.h"
#import "ProjectsViewController.h"
#import "CalculationMethodViewController.h"
#import <math.h>

@interface SimulationViewController ()

//Background Image of category
@property (weak, nonatomic) IBOutlet UIImageView *categoryBackgroundImage;

//Title of the category label
@property (weak, nonatomic) IBOutlet UILabel *categoryTitleLabel;

//Background of project
@property (weak, nonatomic) IBOutlet UIImageView *projectBackgroundImage;

//Project title lable
@property (weak, nonatomic) IBOutlet UILabel *projectTitleLabel;

@end

@implementation SimulationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IS_IPHONE) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.automaticallyAdjustsScrollViewInsets = NO;
    } else {
        UIImage *image = [UIImage imageNamed:@"logo_brasilprev"];
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    }

    //Changing first tab title according to self.lifeProjectType value
    if (self.firstTabButton != nil) {
        NSString* tabTitle = @"";
        if ([self.lifeProjectType isEqualToString:@"forYou"]) {
            tabTitle = [NSString stringWithFormat:@"%@Para Você", tabTitle];
        } else if ([self.lifeProjectType isEqualToString:@"forYourChild"]) {
            tabTitle = [NSString stringWithFormat:@"%@Para Seu Filho", tabTitle];
        }
        NSMutableAttributedString* tabButtonTitle = self.firstTabButton.titleLabel.attributedText.mutableCopy;
        [tabButtonTitle.mutableString replaceCharactersInRange:NSMakeRange(0, tabButtonTitle.mutableString.length) withString:tabTitle];
        [self.firstTabButton setAttributedTitle:tabButtonTitle forState:UIControlStateNormal];
    }
    
    //Changing second tab title according to self.selectedCategory value
    if (self.secondTabButton != nil) {
        NSMutableAttributedString* tabButtonTitle = self.secondTabButton.titleLabel.attributedText.mutableCopy;
        if (self.selectedProject == nil) {
            [tabButtonTitle.mutableString replaceCharactersInRange:NSMakeRange(0, tabButtonTitle.mutableString.length) withString:[self.selectedCategory valueForKey:@"name"]];
        } else {
            [tabButtonTitle.mutableString replaceCharactersInRange:NSMakeRange(0, tabButtonTitle.mutableString.length) withString:[self.selectedProject valueForKey:@"title"]];
        }
        [self.secondTabButton setAttributedTitle:tabButtonTitle forState:UIControlStateNormal];
    }
    
    //Changing third tab title according self.selectedCalculationMethod value
    if (self.thirdTabButton != nil) {
        NSMutableAttributedString* tabButtonTitle = self.thirdTabButton.titleLabel.attributedText.mutableCopy;
        if ([self.selectedCalculationMethod isEqualToString:@"monthlySavings"]) {
            [tabButtonTitle.mutableString replaceCharactersInRange:NSMakeRange(0, tabButtonTitle.mutableString.length) withString:@"Valor Mensal"];
        } else if ([self.selectedCalculationMethod isEqualToString:@"savingTime"]) {
            [tabButtonTitle.mutableString replaceCharactersInRange:NSMakeRange(0, tabButtonTitle.mutableString.length) withString:@"Tempo de Acumulação"];
        }
        [self.thirdTabButton setAttributedTitle:tabButtonTitle forState:UIControlStateNormal];
    }
    
    if (self.selectedCategory != nil) {
        self.categoryTitleLabel.text = [self.selectedCategory valueForKey:@"title"];
        NSString* backgroundImageName = [self.selectedCategory valueForKey:@"background_image"];
        if (!IS_IPHONE) {
            backgroundImageName = [NSString stringWithFormat:@"%@_ipad", backgroundImageName];
        }
        if (![backgroundImageName isEqualToString:@""]) {
            NSString *filePath = [[NSBundle mainBundle] pathForResource:backgroundImageName ofType:@"jpg"];
            self.categoryBackgroundImage.image = [UIImage imageWithContentsOfFile:filePath];
        }

        if ([[self.selectedCategory valueForKey:@"titleColor"] isEqualToString:@"white"]) {
            self.categoryTitleLabel.textColor = [UIColor whiteColor];
            self.categoryTitleLabel.shadowColor = [UIColor darkGrayColor];
        }
    }

    if (self.selectedProject != nil) {
        NSString* backgroundImageName = [self.selectedProject valueForKey:@"background_image"];
        if (!IS_IPHONE) {
            backgroundImageName = [backgroundImageName stringByReplacingOccurrencesOfString:@"iphone_" withString:@"ipad_"];
        }
        if (![backgroundImageName isEqualToString:@""]) {
            NSString *filePath = [[NSBundle mainBundle] pathForResource:backgroundImageName ofType:@"jpg"];
            self.projectBackgroundImage.image = [UIImage imageWithContentsOfFile:filePath];
            
//            self.projectBackgroundImage.image = [UIImage imageNamed:backgroundImageName];
        } else {
            NSManagedObject* category = [self.selectedProject valueForKey:@"category"];
            self.projectBackgroundImage.image = [UIImage imageNamed:[category valueForKey:@"background_image"]];
        }
        
        if ([[self.selectedProject valueForKey:@"titleColor"] isEqualToString:@"white"]) {
            self.projectTitleLabel.textColor = [UIColor whiteColor];
            self.projectTitleLabel.shadowColor = [UIColor darkGrayColor];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)firstTabAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)secondTabTabAction:(UIButton *)sender {
    if ([self isKindOfClass:[ProjectsViewController class]]) {
        for (UIViewController *nav in self.navigationController.viewControllers) {
            if ([nav isKindOfClass:[CategoriesViewController class]]) {
                [self.navigationController popToViewController:nav animated:YES];
            }
        }
    } else {
        for (UIViewController *nav in self.navigationController.viewControllers) {
            if ([nav isKindOfClass:[ProjectsViewController class]]) {
                [self.navigationController popToViewController:nav animated:YES];
            }
        }
    }
    
}

- (IBAction)thirdTabAction:(id)sender {
    for (UIViewController *nav in self.navigationController.viewControllers) {
        if ([nav isKindOfClass:[CalculationMethodViewController class]]) {
            [self.navigationController popToViewController:nav animated:YES];
        }
    }
}

@end
