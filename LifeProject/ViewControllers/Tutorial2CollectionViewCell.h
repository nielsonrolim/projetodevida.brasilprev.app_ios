//
//  Tutorial2CollectionViewCell.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimatableCollectionViewCell.h"

@interface Tutorial2CollectionViewCell : UICollectionViewCell <AnimatableCollectionViewCell>

@property (weak, nonatomic) IBOutlet UIView *elementsView;

- (void) animateCell;
- (void) resetCellState;

@end
