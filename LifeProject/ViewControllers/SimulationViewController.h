//
//  SimulationViewController.h
//  LifeProject
//
//  Created by Nielson Rolim on 10/22/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SimulationViewController : UIViewController

@property (strong, nonatomic) NSString* lifeProjectType;
@property (strong, nonatomic) NSManagedObject* selectedCategory;
@property (strong, nonatomic) NSManagedObject* selectedProject;
@property (strong, nonatomic) NSString* selectedCalculationMethod;

@property (weak, nonatomic) IBOutlet UIButton *firstTabButton;
@property (weak, nonatomic) IBOutlet UIButton *secondTabButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdTabButton;

@end
