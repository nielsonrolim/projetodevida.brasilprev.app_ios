//
//  CategoriesIpadViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/13/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "CategoriesIpadViewController.h"
#import "CategoriesViewController.h"
#import "CategoriesCollectionViewCell.h"
#import "ProjectsViewController.h"
#import <CoreData/CoreData.h>

@interface CategoriesIpadViewController ()

@property (strong, nonatomic) NSArray* categories;

@property (weak, nonatomic) CategoriesViewController* categoriesVC;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation CategoriesIpadViewController

static NSString * const reuseIdentifier = @"CategoriesCollectionViewCell";

- (CategoriesViewController*) categoriesVC {
    if (!_categoriesVC) {
        _categoriesVC = (CategoriesViewController*) self.parentViewController;
    }
    return _categoriesVC;
}

- (NSArray*) categories {
    if (!_categories) {
        _categories = self.categoriesVC.categories;
    }
    return _categories;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"showProjects"]) {
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] firstObject];
        NSManagedObject *selectedCategory = self.categories[indexPath.row];
        ProjectsViewController* projectsVC = (ProjectsViewController*) [segue destinationViewController];
        projectsVC.selectedCategory = selectedCategory;
        projectsVC.lifeProjectType = self.categoriesVC.lifeProjectType;
    }
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.categories.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoriesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
//    cell.lblTeste.text = [self.categories objectAtIndex:indexPath.row];
    
    NSManagedObject* lifeProject = [self.categories objectAtIndex:indexPath.row];
    NSString* lifeProjectTitle = [lifeProject valueForKey:@"name"];
    cell.titleLabel.text = [lifeProjectTitle uppercaseString];
    NSString* buttonImageValue = [NSString stringWithFormat:@"%@_ipad", [lifeProject valueForKey:@"button_image"]];
    cell.background.image = [UIImage imageNamed:buttonImageValue];

    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */


@end
