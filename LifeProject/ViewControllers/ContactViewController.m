//
//  ContactViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/6/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "ContactViewController.h"
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"
#import "MLUtil.h"

@interface ContactViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!IS_IPHONE) {
        UIImage *image = [UIImage imageNamed:@"logo_brasilprev"];
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    }

    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callCustomerService:(UIButton *)sender {
    NSString *phoneNumber = @"tel://08007297170";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (IBAction)callSpecialCustomerService:(UIButton *)sender {
    NSString *phoneNumber = @"tel://08007290150";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (IBAction)sendButtonPressed:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if ([self contactFormIsValid]) {
        [self sendEmail];
    }
}

- (BOOL) contactFormIsValid {
    if ([self.nameTextField.text isEqualToString:@""]) {
        [self showAlertForNameBlank];
        return NO;
    }
    
    if ([self.emailTextField.text isEqualToString:@""]) {
        [self showAlertForEmailBlank];
        return NO;
    }
    
    if (![MLUtil validateEmail:self.emailTextField.text]) {
        [self showAlertForEmailInvalid];
        return NO;
    }
    
    if ([self.messageTextView.text isEqualToString:@""]) {
        [self showAlertForMessageBlank];
        return NO;
    }
    
    return YES;
}

- (void) sendEmail {
    MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSString* emailSender = [NSString stringWithFormat:@"%@ <%@>", self.nameTextField.text, self.emailTextField.text];
    NSString* emailReceiver = @"atendimento@brasilprev.com.br";
    NSString* emailSubject = @"Contato - Brasilprev App iOS";
    NSString* emailContent = [NSString stringWithFormat:@"E-mail enviado através do formulário de contato do aplicativo Brasilprev - versão iOS\n\nRementente: %@ \nE-mail: %@\n\nMensagem:\n%@", self.nameTextField.text, self.emailTextField.text, self.messageTextView.text];
    
    NSString* rawMessage = [NSString stringWithFormat:@"From: %@\nTo: %@\nSubject: %@\n\n%@", emailSender, emailReceiver, emailSubject, emailContent];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSDictionary *parameters = @{
                                 @"key": @"oPdfjC0ohJsIYSML0_UeRA",
                                 @"raw_message": rawMessage
                                 };
    
    [manager POST:@"https://mandrillapp.com/api/1.0/messages/send-raw.json" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        self.nameTextField.text = @"";
        self.emailTextField.text = @"";
        self.messageTextView.text = @"";
        
        //        NSLog(@"JSON: %@", responseObject);
        [progressHUD hideUsingAnimation:YES];
        [self showAlertEmailSent];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        NSLog(@"Error: %@", error);
        [progressHUD hideUsingAnimation:YES];
        [self showAlertEmailNotSent];
    }];
}


- (void) showAlertEmailSent {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Enviado com sucesso"
                                                      message:@"Obrigado por sua mensagem. Entraremos em contato em breve."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertEmailNotSent {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Ocorreu um problema"
                                                      message:@"Desculpe-nos, sua mensagem não foi enviada. Por favor tente novamente daqui a alguns instantes."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertForNameBlank {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"Nome não pode ficar em branco."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertForEmailBlank {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"E-mail não pode ficar em branco."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertForEmailInvalid {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"E-mail não é válido."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertForMessageBlank {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"Mensagem não pode ficar em branco."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
