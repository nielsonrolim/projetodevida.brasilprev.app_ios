//
//  CategoriesTableViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "CategoriesTableViewCell.h"

@implementation CategoriesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
