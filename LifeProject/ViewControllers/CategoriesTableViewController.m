//
//  CategoriesTableViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "CategoriesTableViewController.h"
#import "CategoriesTableViewCell.h"
#import "CategoriesViewController.h"
#import "ProjectsViewController.h"
#import <CoreData/CoreData.h>

@interface CategoriesTableViewController ()

@property (nonatomic, strong) NSArray* categories;

@property (weak, nonatomic) CategoriesViewController* categoriesVC;

@end

@implementation CategoriesTableViewController

- (CategoriesViewController*) categoriesVC {
    if (!_categoriesVC) {
        _categoriesVC = (CategoriesViewController*) self.parentViewController;
    }
    return _categoriesVC;
}

- (NSArray*) categories {
    if (!_categories) {
        _categories = self.categoriesVC.categories;
    }
    return _categories;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.categories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoriesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSManagedObject* lifeProject = [self.categories objectAtIndex:indexPath.row];
    NSString* lifeProjectTitle = [lifeProject valueForKey:@"name"];
    cell.titleLabel.text = [lifeProjectTitle uppercaseString];
    NSString* buttonImageValue = [NSString stringWithFormat:@"%@_iphone", [lifeProject valueForKey:@"button_image"]];
    cell.background.image = [UIImage imageNamed:buttonImageValue];

    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showProjects"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *selectedCategory = self.categories[indexPath.row];
        ProjectsViewController* projectsVC = (ProjectsViewController*) [segue destinationViewController];
        projectsVC.selectedCategory = selectedCategory;
        projectsVC.lifeProjectType = self.categoriesVC.lifeProjectType;
    }
}


@end
