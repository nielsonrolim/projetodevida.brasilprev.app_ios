//
//  CategoriesViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "CategoriesViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface CategoriesViewController ()

@property (weak, nonatomic) AppDelegate* appDelegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end

@implementation CategoriesViewController

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (NSArray*) categories {
    if (!_categories) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Category"];
        
        // Create Predicate
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"type", self.lifeProjectType];
        [fetchRequest setPredicate:predicate];
        
        // Add Sort Descriptor
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        
        NSError *error = nil;
        _categories = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    }
    return _categories;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.firstTabButton.layer.cornerRadius = 3.0f;

    if ([self.lifeProjectType isEqualToString:@"forYourChild"]) {
        self.titleLabel.text = @"Qual o projeto para seu filho?";
        if (!IS_IPAD) {
            self.titleLabel.font = [self.titleLabel.font fontWithSize:18];
        }
        self.subtitleLabel.text = @"Escolha qual destas categorias representa o projeto de vida para seu filho.";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
