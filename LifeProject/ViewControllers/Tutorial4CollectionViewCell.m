//
//  Tutorial4CollectionViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "Tutorial4CollectionViewCell.h"
#import "MLUtil.h"

@interface Tutorial4CollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *figure1ImageView;

@end

@implementation Tutorial4CollectionViewCell

- (void) animateCell {
    int finalXPosition = (IS_IPAD) ? 391 : 62;
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.figure1ImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    
    [UIView animateWithDuration:0.7f
                     animations:^{
                         [MLUtil move:self.figure1ImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) resetCellState {
    int originalXPosition = (IS_IPAD) ? -246 : -166;
    self.figure1ImageView.alpha = 0.5f;
    [MLUtil move:self.figure1ImageView ToX:originalXPosition];
}

@end
