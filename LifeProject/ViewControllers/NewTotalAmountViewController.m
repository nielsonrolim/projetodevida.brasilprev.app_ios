//
//  NewTotalAmountViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/11/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "NewTotalAmountViewController.h"
#import "ResultViewController.h"
#import "MLCurrencyField.h"
#import "MLUtil.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface NewTotalAmountViewController ()

@property (strong, nonatomic) ResultViewController* resultVC;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scroolView;

@end

@implementation NewTotalAmountViewController

- (ResultViewController*) resultVC {
    if (!_resultVC) {
        _resultVC = (ResultViewController*) [self backViewController];
    }
    return _resultVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.totalAmountTextField.text = [MLUtil formatCurrency:self.resultVC.totalAmount];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)confirmButtonPressed:(UIButton *)sender {
    if (self.totalAmountTextField.doubleValue > 0.0) {
        self.resultVC.totalAmount = [NSNumber numberWithDouble:self.totalAmountTextField.doubleValue];
    }
    [self.navigationController popViewControllerAnimated:YES];
}



- (UIViewController *)backViewController
{
    NSInteger myIndex = [self.navigationController.viewControllers indexOfObject:self];
    
    if ( myIndex != 0 && myIndex != NSNotFound ) {
        return [self.navigationController.viewControllers objectAtIndex:myIndex-1];
    } else {
        return nil;
    }
}



@end
