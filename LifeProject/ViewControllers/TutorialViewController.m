//
//  TutorialViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/19/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "TutorialViewController.h"
#import "UITabBarController+CustomColor.h"
#import "UIStoryboard+CheckDevice.h"
#import "Tutorial0CollectionViewCell.h"
#import "AppDelegate.h"
#import "MLUtil.h"


@interface TutorialViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *tutorialBackgroundImageView;
@property (weak, nonatomic) AppDelegate* appDelegate;


@property (nonatomic, strong) NSArray *tutorialImagesNames;

@end

@implementation TutorialViewController

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (NSArray*) tutorialImagesNames {
    if (!_tutorialImagesNames) {
        _tutorialImagesNames = @[@"tutorial-0", @"tutorial-1", @"tutorial-2", @"tutorial-3", @"tutorial-4", @"tutorial-5", @"tutorial-6", @"tutorial-7"];
    }
    return _tutorialImagesNames;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IS_IPHONE && !IS_IPHONE4) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tutorial_bg-iphone5" ofType:@"jpg"];
        self.tutorialBackgroundImageView.image = [UIImage imageWithContentsOfFile:filePath];
    }
}

- (void) viewDidAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.tutorialImagesNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* cellIdentifier;
    // If user already saw the tutorial, get the last object to make the trasition to Main storyboard
    if ((!self.appDelegate.firstRun) || (IS_IPHONE6 || IS_IPHONE6PLUS)) {
        cellIdentifier = @"tutorial-7";
    } else {
        cellIdentifier = [NSString stringWithFormat:@"tutorial-%ld", (long)indexPath.row];
    }

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    //    cell.lblTeste.text = [self.categories objectAtIndex:indexPath.row];
    
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"8.0")) {
        [self collectionView:collectionView willDisplayCell:cell forItemAtIndexPath:indexPath];
    }
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell.reuseIdentifier isEqualToString:[self.tutorialImagesNames lastObject]]) {
        [self.appDelegate startApp:YES];
    }
    
    if ([cell conformsToProtocol:@protocol(AnimatableCollectionViewCell)]) {
        id <AnimatableCollectionViewCell> castedCell = (id <AnimatableCollectionViewCell>) cell;
        
        if (IS_IPHONE4) {
            if (castedCell.elementsView.frame.origin.y >= 100) {
                [MLUtil reposition:castedCell.elementsView increasingXBy:0 andYBy:-80];
            }
        }
        
        [castedCell animateCell];
    }
}


- (void) collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell conformsToProtocol:@protocol(AnimatableCollectionViewCell)]) {
        id <AnimatableCollectionViewCell> castedCell = (id <AnimatableCollectionViewCell>) cell;
        [castedCell resetCellState];
    }
}

#pragma mark <UICollectionViewDelegate>

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */

@end
