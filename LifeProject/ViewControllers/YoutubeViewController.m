//
//  YoutubeViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/22/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "YoutubeViewController.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>

@interface YoutubeViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *brandChannelWebView;

@property (strong, nonatomic) MBProgressHUD *progressHUD;

@end

@implementation YoutubeViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!IS_IPHONE) {
        UIImage *image = [UIImage imageNamed:@"logo_brasilprev"];
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    }
    
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"8.0")) {
        [self initAudio];
    }
    
    NSURL *url = [NSURL URLWithString:@"http://www.youtube.com/user/brasilprev"];
    NSURLRequest* requestObj = [NSURLRequest requestWithURL:url];
    self.brandChannelWebView.delegate = self;
    [self.brandChannelWebView loadRequest:requestObj];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.progressHUD hideUsingAnimation:YES];
}

- (void) initAudio {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    BOOL ok;
    NSError *setCategoryError = nil;
    ok = [audioSession setCategory:AVAudioSessionCategoryPlayback
                             error:&setCategoryError];
    if (!ok) {
        NSLog(@"%s setCategoryError=%@", __PRETTY_FUNCTION__, setCategoryError);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
