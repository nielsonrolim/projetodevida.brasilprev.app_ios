//
//  CalculationMethodViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "CalculationMethodViewController.h"
#import "MonthlySavingsViewController.h"
#import "SavingTimeViewController.h"

@interface CalculationMethodViewController ()

@end

@implementation CalculationMethodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showMonthlySavings"]) {
        MonthlySavingsViewController* monthlySavingsVC = (MonthlySavingsViewController*) [segue destinationViewController];
        monthlySavingsVC.lifeProjectType = self.lifeProjectType;
        monthlySavingsVC.selectedProject = self.selectedProject;
        monthlySavingsVC.selectedCategory = self.selectedCategory;
        monthlySavingsVC.selectedCalculationMethod = @"monthlySavings";
    }

    if ([[segue identifier] isEqualToString:@"showSavingTime"]) {
        SavingTimeViewController* savingTimeVC = (SavingTimeViewController*) [segue destinationViewController];
        savingTimeVC.lifeProjectType = self.lifeProjectType;
        savingTimeVC.selectedProject = self.selectedProject;
        savingTimeVC .selectedCategory = self.selectedCategory;
        savingTimeVC.selectedCalculationMethod = @"savingTime";
    }

}


@end
