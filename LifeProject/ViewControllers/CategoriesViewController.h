//
//  CategoriesViewController.h
//  LifeProject
//
//  Created by Nielson Rolim on 10/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "SimulationViewController.h"

@interface CategoriesViewController : SimulationViewController

@property (nonatomic, strong) NSArray* categories;

@end
