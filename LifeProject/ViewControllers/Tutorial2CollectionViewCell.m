//
//  Tutorial2CollectionViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "Tutorial2CollectionViewCell.h"
#import "MLUtil.h"

@interface Tutorial2CollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *lampImageView;

@property (weak, nonatomic) IBOutlet UIImageView *globoImageView;

@property (weak, nonatomic) IBOutlet UIImageView *planeImageView;

@property (weak, nonatomic) IBOutlet UIImageView *houseImageView;

@property (weak, nonatomic) IBOutlet UIImageView *carImageView;

@property (weak, nonatomic) IBOutlet UIImageView *sunImageView;

@property (weak, nonatomic) IBOutlet UIImageView *cutleryImageView;

@property (weak, nonatomic) IBOutlet UIImageView *compassImageView;

@end



@implementation Tutorial2CollectionViewCell

- (void) animateCell {
    
    //Lamp
    [self animateLamp];
    
    //Globe
    [self animateGlobe];
    
    //Plane
    [self animatePlane];
    
    //House
    [self animateHouse];
    
    //Car
    [self animateCar];
    
    //Sun
    [self animateSun];
    
    //Cutlery
    [self animateCutlery];
    
    //Compass
    [self animateCompass];
}

- (void) resetCellState {
    int originalXPosition;
    
    //Lamp
    originalXPosition = (IS_IPAD) ? 524 : 74;
    self.lampImageView.alpha = 0.0f;
    [MLUtil move:self.lampImageView ToX:originalXPosition];

    //Globe
    originalXPosition = (IS_IPAD) ? 661 : 230;
    self.globoImageView.alpha = 0.0f;
    [MLUtil move:self.globoImageView ToX:originalXPosition];

    //Plane
    originalXPosition = (IS_IPAD) ? 790 : 290;
    self.planeImageView.alpha = 0.0f;
    [MLUtil move:self.planeImageView ToX:originalXPosition];
    
    //House
    originalXPosition = (IS_IPAD) ? 537 : 120;
    self.houseImageView.alpha = 0.0f;
    [MLUtil move:self.houseImageView ToX:originalXPosition];

    //Car
    originalXPosition = (IS_IPAD) ? 609 : 190;
    self.carImageView.alpha = 0.0f;
    [MLUtil move:self.carImageView ToX:originalXPosition];
    
    //Sun
    originalXPosition = (IS_IPAD) ? 765 : 300;
    self.sunImageView.alpha = 0.0f;
    [MLUtil move:self.sunImageView ToX:originalXPosition];
    
    //Cutlery
    originalXPosition = (IS_IPAD) ? 691 : 215;
    self.cutleryImageView.alpha = 0.0f;
    [MLUtil move:self.cutleryImageView ToX:originalXPosition];
    
    //Compass
    originalXPosition = (IS_IPAD) ? 599 : 165;
    self.compassImageView.alpha = 0.0f;
    [MLUtil move:self.compassImageView ToX:originalXPosition];
}

- (void) animateLamp {
    int finalXPosition = (IS_IPAD) ? 386 : 34;
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.lampImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:0.6f
                     animations:^{
                         [MLUtil move:self.lampImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) animateGlobe {
    int finalXPosition = (IS_IPAD) ? 523 : 164;
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.globoImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:1.2f
                     animations:^{
                         [MLUtil move:self.globoImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) animatePlane {
    int finalXPosition;
    
    if (IS_IPAD) {
        finalXPosition = 581;
    } else {
        finalXPosition = 221;
    }
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.planeImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:1.2f
                     animations:^{
                         [MLUtil move:self.planeImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) animateHouse {
    int finalXPosition;
    
    if (IS_IPAD) {
        finalXPosition = 399;
    } else {
        finalXPosition = 55;
    }
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.houseImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:0.8f
                     animations:^{
                         [MLUtil move:self.houseImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) animateCar {
    int finalXPosition;
    
    if (IS_IPAD) {
        finalXPosition = 471;
    } else {
        finalXPosition = 132;
    }
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.carImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:0.9f
                     animations:^{
                         [MLUtil move:self.carImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) animateSun {
    int finalXPosition;
    
    if (IS_IPAD) {
        finalXPosition = 596;
    } else {
        finalXPosition = 229;
    }
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.sunImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:1.3f
                     animations:^{
                         [MLUtil move:self.sunImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) animateCutlery {
    int finalXPosition;
    
    if (IS_IPAD) {
        finalXPosition = 553;
    } else {
        finalXPosition = 180;
    }
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.cutleryImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:1.5f
                     animations:^{
                         [MLUtil move:self.cutleryImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) animateCompass {
    int finalXPosition;
    
    if (IS_IPAD) {
        finalXPosition = 461;
    } else {
        finalXPosition = 101;
    }
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.compassImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
    [UIView animateWithDuration:1.2f
                     animations:^{
                         [MLUtil move:self.compassImageView ToX:finalXPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

@end
