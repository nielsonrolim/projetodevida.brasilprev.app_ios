//
//  SavingTimeViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "SavingTimeViewController.h"
#import "ResultSavingTimeViewController.h"

@interface SavingTimeViewController ()

@property (weak, nonatomic) IBOutlet UITextField *yearTextField;

@property (weak, nonatomic) IBOutlet UITextField *monthTextField;

@end

@implementation SavingTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showResultSavingTime"]) {
        
        ResultSavingTimeViewController* resultSavingTimeVC = (ResultSavingTimeViewController*) [segue destinationViewController];
        resultSavingTimeVC.selectedProject = self.selectedProject;
        resultSavingTimeVC.lifeProjectType = self.lifeProjectType;
        resultSavingTimeVC.selectedCalculationMethod = self.selectedCalculationMethod;
        resultSavingTimeVC.years = [self.yearTextField.text intValue];
        resultSavingTimeVC.months = [self.monthTextField.text intValue];
        resultSavingTimeVC.selectedCategory = self.selectedCategory;
    }
}

- (IBAction)addYearButton:(UIButton *)sender {
    NSInteger currentYear = [self.yearTextField.text intValue];
    currentYear++;
    self.yearTextField.text = [NSString stringWithFormat: @"%d", (int)currentYear];
}

- (IBAction)removeYearButton:(UIButton *)sender {
    NSInteger currentYear = [self.yearTextField.text intValue];
    if (currentYear > 5) {
        currentYear--;
    }
    self.yearTextField.text = [NSString stringWithFormat: @"%d", (int)currentYear];
}

- (IBAction)addMonthButton:(UIButton *)sender {
    NSInteger currentMonth = [self.monthTextField.text intValue];
    if (currentMonth < 11) {
        currentMonth++;
    } else {
        currentMonth = 0;
        [self addYearButton:nil];
    }

    self.monthTextField.text = [NSString stringWithFormat: @"%d", (int)currentMonth];
}

- (IBAction)removeMonthButton:(UIButton *)sender {
    NSInteger currentMonth = [self.monthTextField.text intValue];
    NSInteger currentYear = [self.yearTextField.text intValue];
    
    if (currentMonth > 0) {
        currentMonth--;
    } else {
        if (currentYear > 5) {
            currentMonth = 11;
            [self removeYearButton:nil];
        }
    }
    self.monthTextField.text = [NSString stringWithFormat: @"%d", (int)currentMonth];
}

@end
