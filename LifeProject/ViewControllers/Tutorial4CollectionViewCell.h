//
//  Tutorial4CollectionViewCell.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimatableCollectionViewCell.h"

@interface Tutorial4CollectionViewCell : UICollectionViewCell <AnimatableCollectionViewCell>

@property (weak, nonatomic) IBOutlet UIView *elementsView;

- (void) animateCell;
- (void) resetCellState;

@end
