//
//  HomeViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/24/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "HomeViewController.h"
#import "UIColor+Helper.h"
#import "CategoriesViewController.h"
#import "AppDelegate.h"
#import "MLUtil.h"
#import "UIStoryboard+CheckDevice.h"

@interface HomeViewController ()

@property (strong, nonatomic) NSString* lifeProjectType;

@property (weak, nonatomic) IBOutlet UIView *introView;

@property (weak, nonatomic) IBOutlet UIButton *forYouButton;

@property (weak, nonatomic) IBOutlet UIButton *forYourChildButton;

@property (weak, nonatomic) IBOutlet UIButton *forYouIphone4Button;

@property (weak, nonatomic) IBOutlet UIButton *forYourChildIphone4Button;
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage *image = [UIImage imageNamed:@"logo_brasilprev"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.introView.layer.borderColor = [UIColor colorRgbWithRed:211 green:216 blue:223 alpha:1].CGColor;
    self.introView.layer.borderWidth = 1.0f;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if (IS_IPHONE4) {
        self.forYouIphone4Button.hidden = NO;
        self.forYourChildIphone4Button.hidden = NO;
        
        self.forYouButton.hidden = YES;
        self.forYourChildButton.hidden = YES;
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showCategories"]) {
        CategoriesViewController* categoriesVC = (CategoriesViewController*) [segue destinationViewController];
        categoriesVC.lifeProjectType = self.lifeProjectType;
    }
}
- (IBAction)forYouButtonPressed:(UIButton *)sender {
    self.lifeProjectType = @"forYou";
    [self performSegueWithIdentifier:@"showCategories" sender:self];
}

- (IBAction)forYourChildButtonPressed:(UIButton *)sender {
    self.lifeProjectType = @"forYourChild";
    [self performSegueWithIdentifier:@"showCategories" sender:self];
}

@end
