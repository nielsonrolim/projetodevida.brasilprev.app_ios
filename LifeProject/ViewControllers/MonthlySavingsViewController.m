//
//  MonthlySavingsViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/6/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "MonthlySavingsViewController.h"
#import "ResultMonthlySavingViewController.h"
#import "MLCurrencyField.h"

@interface MonthlySavingsViewController ()

@property (weak, nonatomic) IBOutlet MLCurrencyField *monthlyInvestmentTextField;

@end

@implementation MonthlySavingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.monthlyInvestmentTextField.maximumValueAllowed = [@999999999 doubleValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showResultMonthlySavings"]) {
        ResultMonthlySavingViewController* resultMonthlySavingsVC = (ResultMonthlySavingViewController*) [segue destinationViewController];
        resultMonthlySavingsVC.selectedProject = self.selectedProject;
        resultMonthlySavingsVC.lifeProjectType = self.lifeProjectType;
        resultMonthlySavingsVC.selectedCalculationMethod = self.selectedCalculationMethod;
        resultMonthlySavingsVC.monthlyInvestment = self.monthlyInvestmentTextField.doubleValue;
        resultMonthlySavingsVC.selectedCategory = self.selectedCategory;
    }
}

- (IBAction)performSegueIfValid:(id)sender {
    if ([self monthlyInvestmentTextFieldIsValid]) {
        [self performSegueWithIdentifier:@"showResultMonthlySavings" sender:self];
    }
}

- (BOOL) monthlyInvestmentTextFieldIsValid {
    if ([self.monthlyInvestmentTextField.text isEqualToString:@""] || self.monthlyInvestmentTextField.doubleValue <= 0.0) {
        [self showAlertForEmptyMonthlyInvestmentTextField];
        return NO;
    }
    
    if (self.monthlyInvestmentTextField.doubleValue > [[self.selectedProject valueForKey:@"value"] doubleValue]) {
        [self showAlertForMonthlyInvestmentBiggerThanProjectValue];
        return NO;
    }
    return YES;
}

- (void) showAlertForEmptyMonthlyInvestmentTextField {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"Este valor mensal é inválido."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertForMonthlyInvestmentBiggerThanProjectValue {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"O valor mensal deve ser menor que o valor do projeto de vida."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

@end
