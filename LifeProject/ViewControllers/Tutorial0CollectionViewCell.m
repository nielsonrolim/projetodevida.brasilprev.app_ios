//
//  Tutorial0CollectionViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "Tutorial0CollectionViewCell.h"

@interface Tutorial0CollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end

@implementation Tutorial0CollectionViewCell

- (void) animateCell {
    [UIView animateWithDuration:1.5f
                     animations:^{
                         [self.logoImageView setAlpha:1.0f];
                     }
     ];
}

- (void) resetCellState {
    self.logoImageView.alpha = 0.0f;
}

@end
