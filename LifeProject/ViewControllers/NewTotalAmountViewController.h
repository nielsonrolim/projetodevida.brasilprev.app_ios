//
//  NewTotalAmountViewController.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/11/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "SimulationViewController.h"
#import "MLCurrencyField.h"

@interface NewTotalAmountViewController : SimulationViewController

@property (weak, nonatomic) IBOutlet MLCurrencyField *totalAmountTextField;

@end
