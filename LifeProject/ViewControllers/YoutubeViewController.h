//
//  YoutubeViewController.h
//  LifeProject
//
//  Created by Nielson Rolim on 10/22/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YoutubeViewController : UIViewController <UIWebViewDelegate>

@end
