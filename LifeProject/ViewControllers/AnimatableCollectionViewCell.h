//
//  AnimatableCollectionViewCell.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol AnimatableCollectionViewCell <NSObject>

@property (weak, nonatomic) IBOutlet UIView *elementsView;

- (void) animateCell;
- (void) resetCellState;

@end


