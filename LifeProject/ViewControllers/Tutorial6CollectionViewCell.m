//
//  Tutorial6CollectionViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/20/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "Tutorial6CollectionViewCell.h"
#import "AppDelegate.h"
#import "MLUtil.h"

@interface Tutorial6CollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *logoBrasilprevImageView;

@property (weak, nonatomic) IBOutlet UIButton *finishButton;

@property (weak, nonatomic) AppDelegate* appDelegate;

@property (nonatomic, strong) NSArray *tutorialImagesNames;

@end


@implementation Tutorial6CollectionViewCell

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (void) animateCell {
    [UIView animateWithDuration:1.5f
                     animations:^{
                         [self.logoBrasilprevImageView setAlpha:1.0f];
                     }
     ];

    [UIView animateWithDuration:1.9f
                     animations:^{
                         [self.finishButton setAlpha:1.0f];
                     }
     ];

}

- (void) resetCellState {
    self.logoBrasilprevImageView.alpha = 0.0f;
    self.finishButton.alpha = 0.0f;
}

- (IBAction)finishButtonPressed:(UIButton *)sender {
    [self.appDelegate startApp:YES];
}

@end
