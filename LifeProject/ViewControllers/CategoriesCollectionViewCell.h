//
//  CategoriesCollectionViewCell.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/13/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
