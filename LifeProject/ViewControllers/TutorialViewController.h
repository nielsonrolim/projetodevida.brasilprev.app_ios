//
//  TutorialViewController.h
//  LifeProject
//
//  Created by Nielson Rolim on 11/19/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
