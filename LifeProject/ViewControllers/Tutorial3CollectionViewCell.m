//
//  Tutorial3CollectionViewCell.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "Tutorial3CollectionViewCell.h"
#import "MLUtil.h"

@interface Tutorial3CollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *image1ImageView;

@end

@implementation Tutorial3CollectionViewCell

- (void) animateCell {
    int finalYPosition = (IS_IPAD) ? 215 : 90;
    
    [UIView animateWithDuration:1.0f
                     animations:^{
                         [self.image1ImageView setAlpha:1.0f];
                     }
                     completion:^(BOOL finished) {
                     }
     ];

    [UIView animateWithDuration:1.0f
                     animations:^{
                         [MLUtil move:self.image1ImageView ToY:finalYPosition];
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

- (void) resetCellState {
    int originalYPosition = (IS_IPAD) ? 470 : 259;
    
    self.image1ImageView.alpha = 0.0f;
    [MLUtil move:self.image1ImageView ToY:originalYPosition];
}

@end
