//
//  ProjectsTableViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 10/25/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "ProjectsTableViewController.h"
#import "ProjectsViewController.h"
#import "CalculationMethodViewController.h"
#import "AppDelegate.h"
#import "MLUtil.h"
#import <CoreData/CoreData.h>

@interface ProjectsTableViewController ()

@property (strong, nonatomic) NSArray* projetcs;

@property (weak, nonatomic) ProjectsViewController* projectsVC;

@property (weak, nonatomic) AppDelegate* appDelegate;

@end

@implementation ProjectsTableViewController

- (NSArray*) projects {
    if (!_projetcs) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
        _projetcs = [[self.projectsVC.selectedCategory valueForKey:@"projects"] sortedArrayUsingDescriptors:@[sortDescriptor]];
    }
    return _projetcs;
}

- (ProjectsViewController*) projectsVC {
    if (!_projectsVC) {
        _projectsVC = (ProjectsViewController*) self.parentViewController;
    }
    return _projectsVC;
}

- (AppDelegate*) appDelegate {
    if (!_appDelegate) {
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.projects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProjectsCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [self.projetcs[indexPath.row] valueForKey:@"title"];
    cell.detailTextLabel.text = [MLUtil formatCurrency:[self.projetcs[indexPath.row] valueForKey:@"value"] hasDecimal:NO];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"showCalculationMethod"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *selectedProject = self.projetcs[indexPath.row];
        CalculationMethodViewController* calculationMethodVC = (CalculationMethodViewController*) [segue destinationViewController];
        calculationMethodVC.selectedProject = selectedProject;
        calculationMethodVC.lifeProjectType = self.projectsVC.lifeProjectType;
        calculationMethodVC.selectedCategory = self.projectsVC.selectedCategory;
    }
}


@end
