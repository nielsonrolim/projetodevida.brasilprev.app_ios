//
//  ResultViewController.m
//  LifeProject
//
//  Created by Nielson Rolim on 11/10/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import "ResultViewController.h"
#import "MLUtil.h"
#import <math.h>
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"

#import "ProjectsViewController.h"
#import "SavingTimeViewController.h"
#import "MonthlySavingsViewController.h"
#import "NewTotalAmountViewController.h"


#import <FacebookSDK/FacebookSDK.h>

@interface ResultViewController ()

//Life Project Name
@property (weak, nonatomic) IBOutlet UILabel *projectNameLabel;

//Total months
@property (assign, nonatomic) int totalMonths;

//Total amount label in button
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;

//Monthly investment in button
@property (weak, nonatomic) IBOutlet UILabel *monthlyInvestmentLabel;

//Saving time label
@property (weak, nonatomic) IBOutlet UILabel *savingTimeLabel;

//Total amount label in intro text
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabelInIntroText;

//Monthly investment label in intro text
@property (weak, nonatomic) IBOutlet UILabel *monthlyInvestmentLabelInIntroText;

//Saving time label in intro text
@property (weak, nonatomic) IBOutlet UILabel *savingTimeLabelInIntroText;

//Investment Slider
@property (weak, nonatomic) IBOutlet UISlider *investmentSlider;

//Amount Contributed Label
@property (weak, nonatomic) IBOutlet UILabel *amountContributedLabel;

//Income Label
@property (weak, nonatomic) IBOutlet UILabel *incomeLabel;

@end

@implementation ResultViewController

- (NSNumber*) totalAmount {
    if (!_totalAmount) {
        if (self.selectedProject != nil) {
            _totalAmount = [self.selectedProject valueForKey:@"value"];
        } else {
            _totalAmount = [NSNumber numberWithInt:0];
        }
    }
    return _totalAmount;
}

- (int) totalMonths {
    if (!_totalMonths) {
        _totalMonths = (self.years * 12) + self.months;
    }
    return _totalMonths;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IS_IPAD) {
        self.projectNameLabel.text = [NSString stringWithFormat:@"%@ - %@", [[self.selectedProject valueForKey:@"category"] valueForKey:@"name"], [self.selectedProject valueForKey:@"title"]];
    }
    
    self.totalAmountLabel.text = [MLUtil formatCurrency:self.totalAmount hasDecimal:NO];
    self.totalAmountLabelInIntroText.text = self.totalAmountLabel.text;
    
    self.monthlyInvestmentLabel.text = [MLUtil formatCurrency:[NSNumber numberWithDouble:self.monthlyInvestment]];
    self.monthlyInvestmentLabelInIntroText.text = self.monthlyInvestmentLabel.text;
    
    self.savingTimeLabel.text = [self savingTimeToString];
    self.savingTimeLabelInIntroText.text = self.savingTimeLabel.text;
    
    self.monthlyInterestRate = [self convertAnualToMonthlyInterestRate:[self.anualInterestRateSlider value]];
    
    [self.investmentSlider setThumbImage:[[UIImage alloc] init] forState:UIControlStateNormal];
    
}

- (void) viewWillAppear:(BOOL)animated {
    [self viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"newTotalAmountFromMonthlySaving"] || [[segue identifier] isEqualToString:@"newTotalAmountFromSavingTime"]) {
        
        NewTotalAmountViewController* newTotalAmountVC = (NewTotalAmountViewController*) [segue destinationViewController];
        newTotalAmountVC.selectedProject = self.selectedProject;
        newTotalAmountVC.lifeProjectType = self.lifeProjectType;
        newTotalAmountVC.selectedCalculationMethod = self.selectedCalculationMethod;
        newTotalAmountVC.totalAmountTextField.text = [MLUtil formatCurrency:self.totalAmount];
        newTotalAmountVC.selectedCategory = self.selectedCategory;
    }
}


- (IBAction)anualInterestRateChanged:(UISlider *)sender {
    double monthlyInterestRate = round([sender value]);
    self.anualInterestRateLabel.text = [NSString stringWithFormat:@"%.0f%%", monthlyInterestRate];
    self.monthlyInterestRate = [self convertAnualToMonthlyInterestRate:monthlyInterestRate];
}

- (NSString*) savingTimeToString {
    if (self.months > 0) {
        if (self.months == 1) {
            return [NSString stringWithFormat:@"%d anos e %d mês", self.years, self.months];
        } else {
            return [NSString stringWithFormat:@"%d anos e %d meses", self.years, self.months];
        }
    } else {
        return [NSString stringWithFormat:@"%d anos", self.years];
    }
}

- (void) totalMonthsToYearsAndMonths {
    self.years = self.totalMonths / 12;
    self.months = self.totalMonths % 12;
}

- (double) convertAnualToMonthlyInterestRate:(double)anualInterestRate {
    //Rounding anual interestRate
    anualInterestRate = round(anualInterestRate*10.0)/10.0;
    
    //Applying algoritm
    double monthlyInterestRate =  pow((1.0 + anualInterestRate/100.0), (1.0/12.0)) - 1.0;
    
    return monthlyInterestRate;
}

- (void) calculateMonthlyInvestiment {
    double result = [self.totalAmount doubleValue] * ( self.monthlyInterestRate / ( pow((1.0 + self.monthlyInterestRate), self.totalMonths * 1.0)  - 1.0) );
    
    self.monthlyInvestmentLabel.text = [MLUtil formatCurrency:[NSNumber numberWithDouble:result]];
    self.monthlyInvestmentLabelInIntroText.text = self.monthlyInvestmentLabel.text;
    
    double amountContributed = result * (self.totalMonths * 1.0);
    
    self.amountContributedLabel.text = [MLUtil formatCurrency:[NSNumber numberWithDouble:amountContributed]];
    self.incomeLabel.text =  [MLUtil formatCurrency:[NSNumber numberWithDouble:[self.totalAmount doubleValue] - amountContributed]];
    
    self.investmentSlider.maximumValue = [self.totalAmount doubleValue];
    self.investmentSlider.value = amountContributed;
}

- (void) calculateSavingTime {
    
    double result = -1.0 * ( log( (self.monthlyInvestment - self.monthlyInterestRate * 0.0) / ( self.monthlyInvestment + self.monthlyInterestRate * [self.totalAmount doubleValue]) ) / log( self.monthlyInterestRate + 1.0 ) ) / 12.0;
    
//    NSLog(@"result: %f", round(result*100)/100);
    
    self.totalMonths = round((round(result*100)/100) * 12.0);
    
//    NSLog(@"self.totalMonths: %d", self.totalMonths);
    
    [self totalMonthsToYearsAndMonths];
    
    self.savingTimeLabel.text = [self savingTimeToString];
    self.savingTimeLabelInIntroText.text = self.savingTimeLabel.text;
    
    double amountContributed = (self.monthlyInvestment * 12.0) * result;
    
    self.amountContributedLabel.text = [MLUtil formatCurrency:[NSNumber numberWithDouble:amountContributed]];
    self.incomeLabel.text =  [MLUtil formatCurrency:[NSNumber numberWithDouble:[self.totalAmount doubleValue] - amountContributed]];
    
    self.investmentSlider.maximumValue = [self.totalAmount doubleValue];
    self.investmentSlider.value = amountContributed;
}


- (IBAction)newSimulationAction:(UIButton *)sender {
    [self showConfirmAlertFoNewSimulation];
}

- (IBAction)editSavingTimeButton:(UIButton *)sender {
    for (UIViewController *nav in self.navigationController.viewControllers) {
        if ([nav isKindOfClass:[SavingTimeViewController class]]) {
            [self.navigationController popToViewController:nav animated:YES];
        }
    }
}

- (IBAction)editMonthlyInvestmentButton:(UIButton *)sender {
    for (UIViewController *nav in self.navigationController.viewControllers) {
        if ([nav isKindOfClass:[MonthlySavingsViewController class]]) {
            [self.navigationController popToViewController:nav animated:YES];
        }
    }
}

- (IBAction)facebookShareButtonPressed:(UIButton *)sender {
    // Check if the Facebook app is installed and we can present the share dialog
    
    NSString* shareLinkPath = @"http://www.brasilprev.com.br/";
    NSURL* shareLink = [NSURL URLWithString:shareLinkPath];
    
    NSString* shareName = @"Meu Projeto de Vida";
    NSString* shareCaption = @"APP Brasilprev";
    NSString* shareDescription = @"Já sei o valor e o tempo que preciso para realizar o meu projeto de vida. Baixe o APP da Brasilprev e saiba o seu também.";
    
    NSString* imageName = [self.selectedProject valueForKey:@"background_image"];
    imageName = [imageName stringByReplacingOccurrencesOfString:@"iphone_" withString:@"fb_"];
    
    NSString* sharePicturePath = [NSString stringWithFormat:@"http://mobilife.com.br/projetos/brasilprev/fb/%@.jpg", imageName];
    NSLog(@"sharePicturePath: %@", sharePicturePath);
    NSURL* sharePicture = [NSURL URLWithString:sharePicturePath];
    
    FBLinkShareParams *params = [[FBLinkShareParams alloc] initWithLink:shareLink
                                                                   name:shareName
                                                                caption:shareCaption
                                                            description:shareDescription
                                                                picture:sharePicture];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present share dialog
        [FBDialogs presentShareDialogWithParams:params clientState:nil
                                        handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                            if (error) {
                                                // An error occurred, we need to handle the error
                                                // See: https://developers.facebook.com/docs/ios/errors
                                                NSLog(@"Error publishing story: %@", error.description);
                                            } else {
                                                // Success
                                                // NSLog(@"result %@", results);
                                            }
                                        }];
    } else {
        // Present the feed dialog
        
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       shareName, @"name",
                                       shareCaption, @"caption",
                                       shareDescription, @"description",
                                       shareLinkPath, @"link",
                                       sharePicturePath, @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          // NSLog(@"Error publishing story: %@", error.description);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                              // NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  // NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  // NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  // NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


- (void) showConfirmAlertFoNewSimulation {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Nova Simulação"
                                                      message:@"Você deseja iniciar uma nova simulação?"
                                                     delegate:self
                                            cancelButtonTitle:@"Não"
                                            otherButtonTitles:@"Sim", nil];
    message.tag = 1;
    [message show];
}

- (void) showDialogToGetEmailAddress {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"E-mail por e-mail"
                                                     message:@"Digite seu e-mail abaixo:"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancelar"
                                           otherButtonTitles:@"Enviar", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = 2;
    [alert show];
}

- (void) showAlertForEmailBlank {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"E-mail não pode ficar em branco."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertForEmailInvalid {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Atenção!"
                                                      message:@"E-mail não é válido."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void) showAlertSimulationSentToEmail:(NSString*)emailAddress {
    NSString* message = [NSString stringWithFormat:@"Sua simulação foi enviada para %@", emailAddress];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Simulação enviada"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) showAlertSimulationSentToEmailFailure:(NSString*)emailAddress {
    NSString* message = [NSString stringWithFormat:@"Sua simulação não foi enviada para %@", emailAddress];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ocorreu um erro"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        switch(buttonIndex) {
            case 0: //"No" pressed
                //do something?
                break;
            case 1: //"Yes" pressed
                //here you pop the viewController
                [self.navigationController popToRootViewControllerAnimated:YES];
                break;
        }
    }
    
    if (alertView.tag == 2) {
        NSString* emailAddress = [[alertView textFieldAtIndex:0] text];
        switch(buttonIndex) {
            case 0: //"No" pressed
                //do something?
                break;
            case 1: //"Yes" pressed
                //here you pop the viewController
                [self sendSimulationToEmail:emailAddress];
                break;
        }
    }
}

- (IBAction)sendByEmailButtonPressed:(UIButton *)sender {
    [self showDialogToGetEmailAddress];
}

- (void) sendSimulationToEmail:(NSString*)emailAddress {
    if ([self validateEmail:emailAddress]) {
        
        MBProgressHUD *progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSString* emailSender = @"Brasilprev <atendimento@brasilprev.com.br>";
        NSString* emailReceiver = emailAddress;
        NSString* emailSubject = @"Seu Projeto de Vida - Brasilprev App iOS";
        NSString* emailContent = [NSString stringWithFormat:@"\
Simulação de Projeto de Vida realizada através do aplicativo Brasilprev - versão iOS\n\
                                  \n\
Projeto: %@\n\
Valor Total: %@\n\
Tempo de Acumulação: %@\n\
Investimento mensal: %@\n\
Taxa de rentabilidade: %@\n\
Valor investido: %@\n\
Rendimentos: %@\n\
",
                                  [self.selectedProject valueForKey:@"title"],
                                  self.totalAmountLabel.text,
                                  self.savingTimeLabel.text,
                                  self.monthlyInvestmentLabel.text,
                                  self.anualInterestRateLabel.text,
                                  self.amountContributedLabel.text,
                                  self.incomeLabel.text
                                  ];
        
        NSString* rawMessage = [NSString stringWithFormat:@"From: %@\nTo: %@\nSubject: %@\n\n%@", emailSender, emailReceiver, emailSubject, emailContent];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        NSDictionary *parameters = @{
                                     @"key": @"oPdfjC0ohJsIYSML0_UeRA",
                                     @"raw_message": rawMessage
                                     };
        
        [manager POST:@"https://mandrillapp.com/api/1.0/messages/send-raw.json" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            //        NSLog(@"JSON: %@", responseObject);
            [progressHUD hideUsingAnimation:YES];
            [self showAlertSimulationSentToEmail:emailAddress];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //        NSLog(@"Error: %@", error);
            [progressHUD hideUsingAnimation:YES];
            [self showAlertSimulationSentToEmailFailure:emailAddress];
        }];
        
    }
}

- (BOOL) validateEmail:(NSString*)emailAddress {
    if ([emailAddress isEqualToString:@""]) {
        [self showAlertForEmailBlank];
        return NO;
    } else  if (![MLUtil validateEmail:emailAddress]) {
        [self showAlertForEmailInvalid];
        return NO;
    }
    return YES;
}

@end
