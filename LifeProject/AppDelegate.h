//
//  AppDelegate.h
//  LifeProject
//
//  Created by Nielson Rolim on 10/21/14.
//  Copyright (c) 2014 Mobilife. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

//Core Data properties
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

//First Run
@property (assign, nonatomic) BOOL firstRun;

//Core Data methods
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

//Start Main App
- (void)startApp:(BOOL)animated;



@end

